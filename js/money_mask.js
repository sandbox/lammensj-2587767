/**
 * @file
 * money_mask.js
 */

(function ($) {

  "use strict";

  Drupal.behaviors.money_mask = {
    attach: function (context, settings) {
      var config = settings.money_mask;

      if ($.fn.maskMoney) {
        var self = this;
        for (var element in config.elements) {
          if (config.elements[element].hasOwnProperty('id')) {
            var $el = $('#' + config.elements[element].id, context);
            $el.once('money_mask', self.initMaskMoney($el, config.options));
          }
        }
      }
    },

    initMaskMoney: function (el, options) {
      el.maskMoney(options);
      el.maskMoney('mask');
    }
  };
})(jQuery);
